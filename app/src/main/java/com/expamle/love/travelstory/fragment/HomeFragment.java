package com.expamle.love.travelstory.fragment;

import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.LinearLayout;

import com.expamle.love.travelstory.R;
import com.expamle.love.travelstory.sql.HomeReadFireBase;

public class HomeFragment extends Fragment {


    private static final String TAG = HomeFragment.class.getSimpleName();
    private RecyclerView recyclerView;
    private Toolbar toolbar;
    private View view;
    private AppCompatActivity activity;
    private BackDrop backDrop;
    private NestedScrollView scrollView;
    private LinearLayout linearLayout;
    private Drawable openDrawable;
    private Drawable closeDrawable;
    private AccelerateDecelerateInterpolator interpolator;
    private HomeReadFireBase homeReadFireBase;


    //設定menu
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
//        設定 BackDrop
        setBackDrop();


        return view;

    }

    private void setBackDrop() {
        setView();
        backDrop = new BackDrop(activity, toolbar, scrollView, linearLayout, openDrawable, closeDrawable, interpolator);
    }

    private void setView() {
        activity = (AppCompatActivity) getContext();
        toolbar = view.findViewById(R.id.home_toolbar);
        scrollView = view.findViewById(R.id.home_backdrop_scroll);
        linearLayout = view.findViewById(R.id.home_backdrop_position);
        openDrawable = ContextCompat.getDrawable(activity, R.drawable.navigation_icon_open);
        closeDrawable = ContextCompat.getDrawable(activity, R.drawable.navigation_icon_close);
        interpolator = new AccelerateDecelerateInterpolator();
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        findViews();


        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.set(8, 8, 8, 0);
            }
        });

        homeReadFireBase.getAdapter().startListening();
    }


    //      綁定
    private void findViews() {
        recyclerView = view.findViewById(R.id.home_recycler);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new GridLayoutManager(activity, 1));
//        readFireBaseData
        homeReadFireBase = new HomeReadFireBase(activity);
        homeReadFireBase.readFireBaseData();

        recyclerView.setAdapter(homeReadFireBase.getAdapter());


    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();
    }




    //  設定menu
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.home_menu, menu);
    }


}

